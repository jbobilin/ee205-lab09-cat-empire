///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   29 April 2021
///////////////////////////////////////////////////////////////////////////////

// *************************
//    UNCOMMENT TO DEBUG
//    #define DEBUG
// *************************


#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}



std::vector<std::string> Cat::names;

void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}



Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}



bool CatEmpire::empty() {
   if (topCat == nullptr) {
      return true;
   } else {
      return false;
   }
}



void CatEmpire::addCat( Cat* newCat) {
   assert(newCat != nullptr);

   newCat -> left = nullptr;
   newCat -> right = nullptr;

   if(topCat == nullptr) {
      topCat = newCat;
      return;
   }

   addCat(topCat, newCat);
}



void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {
   //check if empty
   assert(atCat != nullptr);
   assert(newCat != nullptr);

   // if new < current cat node
   if(atCat -> name > newCat -> name) {
      if(atCat -> left == nullptr) {
         atCat -> left = newCat;
      }
      else {
         addCat(atCat -> left, newCat);
      }
   }

   // if new > current cat node 
   if(atCat -> name < newCat -> name) {
      if(atCat -> right == nullptr) {
         atCat -> right = newCat;
      }
      else {
         addCat(atCat -> right, newCat);
      }
   }
}



void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
   
   dfsInorderReverse( topCat, 1 );
}



void CatEmpire::dfsInorderReverse ( Cat* atCat, int depth ) const {
   if( atCat == nullptr )
      return;

   dfsInorderReverse( atCat -> right, depth + 1 );
   cout << string( 6 * (depth - 1), ' ' ) << atCat -> name;

   //if leaf node
   if ( (atCat -> left == nullptr) && (atCat -> right == nullptr) )
         cout << endl;

   //if has two children
   if( (atCat -> left != nullptr) && (atCat -> right != nullptr) )
      cout << "<" << endl;

   //if has right child only
   if( (atCat -> left != nullptr) && (atCat -> right == nullptr) )
      cout << "\\" << endl;

   //if hase left child only
   if( (atCat -> left == nullptr) && (atCat -> right != nullptr) )
      cout << "/" << endl;
   
   //do left
   dfsInorderReverse( atCat -> left, depth + 1 );

}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}



void CatEmpire::dfsInorder ( Cat* atCat ) const {

   if ( atCat == nullptr )
      return;

   //go to next and print
   dfsInorder( atCat -> left);
   cout << atCat -> name << endl;

   //recurse
   dfsInorder( atCat -> right);


}



void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}



void CatEmpire::dfsPreorder ( Cat* atCat ) const {

   if( atCat == nullptr )
      return;

   //if leaf node then do nothing

   //if has two children
   if( (atCat -> left != nullptr) && (atCat -> right != nullptr) )
      cout << atCat -> name << " begat " << atCat -> left -> name << " and " << atCat -> right -> name << endl;

   //if has right child only
   if( (atCat -> left != nullptr) && (atCat -> right == nullptr) )
      cout << atCat -> name << " begat " << atCat -> left -> name << endl;

   //if hase left child only
   if( (atCat -> left == nullptr) && (atCat -> right != nullptr) )
      cout << atCat -> name << " begat " << atCat -> right -> name << endl;
   
   //move dwn a generation
   dfsPreorder( atCat -> left );
   dfsPreorder( atCat -> right );

}



void CatEmpire::catGenerations() const {
//debug ordinal num fn
#ifdef DEBUG
   cout << "debug getEnglishSuffix" << endl;
   int i;
   for (i=0;i<500;i++) {
      cout << i;
      getEnglishSuffix(i);
      cout << endl;
   }
#endif

   //init vars
   int genNum = 1;
   int catsInThisGen = 1;
   int catsInNextGen = 0;

   //create queue
   queue<Cat*> catQueue;
   catQueue.emplace(topCat);

   // print text for 1st gen
   cout << genNum;
   getEnglishSuffix(genNum);
   cout << " Generation" << endl << "  ";
 
   // enter loop
   while ( catQueue.empty() == false ) {
      //remove
      Cat* cat = catQueue.front();
      catQueue.pop();
   
      // if no more cats then go to next generation and print
      if ( (catsInThisGen == 0) ){
         genNum++;
         cout << endl << genNum;
         getEnglishSuffix(genNum);
         cout << " Generation" << endl << "  ";
         catsInThisGen = catsInNextGen;
         catsInNextGen = 0;
      }

      // if no more cats terminate loop
      if ( cat == nullptr )
         return;

      //push and increment next gen
      if ( cat -> left != nullptr ) {
         catQueue.push( cat -> left );
         catsInNextGen++;
      }

      if ( cat -> right != nullptr ) {
         catQueue.push( cat -> right );
         catsInNextGen++;
      }
      
      //print cat name
      cout << cat -> name << "  ";

      //decrement current gen
      catsInThisGen--;
   }
   
   cout << endl;
}



void CatEmpire::getEnglishSuffix(int n) const {

   //if 10s digit is 1
   if ((n / 10 ) % 10 == 1) {
      cout << "th";
      return;
   }

   //else  if unit digit is 0 or 4-9 print th
   if ( ((n % 10) == 0) || (((n % 10) <= 9) && ((n % 10) >= 4) ) )
      cout << "th";
   
   //else if unit digit is 1 print "st"
   if ( (n % 10) == 1)
      cout << "st";

   //else if unit digit is 2 print "nd"
   if ( (n % 10) == 2)
      cout << "nd";

   //else if unit digit is 3 print "rd"
   if ( (n % 10) == 3)
      cout << "rd";
}
