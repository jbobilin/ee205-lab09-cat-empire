///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   29 April 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

class Cat {
private:    /// Member variables
	std::string name;

public:     /// Constructors
	Cat( const std::string newName );
	friend class CatEmpire;

private:    /// Private methods
	void setName( const std::string newName ); // Name is a key in our BST, so we don't
		                                        // want to let people change it as it'll
		                                        // mess up the tree.
protected:
   Cat* left = nullptr;
   Cat* right = nullptr;

private:    /// Static variables
	static std::vector<std::string> names;

public:     /// Static methods
	static void initNames();
	static Cat* makeCat();
};


class CatEmpire {
private:
	Cat* topCat = nullptr;

public:
	bool empty();  // Return true if empty

public:
	void addCat( Cat* newCat );   // Add a cat starting at the root 
	void catFamilyTree() const;   // print sideways tree using DFS reverse traversal
	void catList() const;         // list alphabetically using DFS traversal
	void catBegat() const;        // print pedigree using preorder traversal
	void catGenerations() const;  // print generations using BFS traversal

private:
	void addCat( Cat* atCat, Cat* newCat );                  // Add a cat starting at atCat
	void dfsInorderReverse( Cat* atCat, int depth ) const;   // supports catFamilyTree
	void dfsInorder( Cat* atCat ) const;                     // supports catList
	void dfsPreorder( Cat* atCat ) const;                    // supports catBegat
   void getEnglishSuffix( int n ) const;                    // supports catGenerations

};
